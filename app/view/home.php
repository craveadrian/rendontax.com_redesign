<div id="welcome">
	<div class="row">
		<h2>Welcome</h2>
		<div class="container">
			<div class="left">
				<div class="panel first">
					<p>Le conseguiremos el máximo reembolso posible.</p>
				</div>
				<div class="panel second">
					<p>Puede calificar para ciertos créditos aunque no tenga seguro social valido.</p>
				</div>
				<div class="panel third">
					<p>Le ayudaremos a conseguir un número de identificación (ITIN)</p>
				</div>
			</div>
			<div class="mid">
				<img src="public/images/content/welcome-img.jpg" alt="Welcome Image">
				<div class="button">
					<a href="contact#content" class="btn2">Contact Us</a>
				</div>
			</div>
			<div class="right">
				<div class="panel first">
					<p>Preparamos los impuestos de años anteriores.</p>
				</div>
				<div class="panel second">
					<p>Si usted es contratista o dueño de negocio, le llevamos la contabilidad y preparamos cheques para sus trabajadores “Payroll” semanales o quincenales.</p>
				</div>
				<div class="panel third">
					<p>Se preparan todas las formas trimestrales, W’2s, 1099’s etc. etc. para sus trabajadores.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="about">
	<div class="row">
		<h1>RENDON INCOME TAX</h1>
		<p>Nuestro negocio está creciendo, y por lo tanto, necesitamos más espacio. Tenemos el placer de anunciar nuestra nueva ubicación en Grand Prairie y algunos nuevos servicios que ahora ofrecemos a nuestros clientes. Vamos a servir a todos en su Preparación de Impuestos, Servicios de Contabilidad, y Notaría , con el mismo profesionalismo y experiencia de siempre. Hemos añadido un Servicio de Bienes Raíces en nuestra oficina. Podemos ayudarle a encontrar un nuevo hogar o vender el actual. Sólo tiene que preguntar cómo empezar. Tenemos conexiones con muchas Instituciones Financieras y podemos ayudarle a obtener su préstamo hoy.</p>
		<a href="#" class="btn2">Learn more</a>
		<h3>EXPERIENCIA Y PROFESIONALISMO</h3>
	</div>
</div>
<div id="services">
	<div class="row">
		<div class="service">
			<img src="public/images/content/service1.jpg" alt="ITIN’S">
			<div class="links">
				<h4>ITIN’S</h4>
				<a href="services#service1" class="btn2">Learn more</a>
			</div>
		</div>
		<div class="service">
			<img src="public/images/content/service2.jpg" alt="PAYROLL">
			<div class="links">
				<h4>PAYROLL</h4>
				<a href="services#service2" class="btn2">Learn more</a>
			</div>
		</div>
		<div class="service">
			<img src="public/images/content/service3.jpg" alt="PREPARACIÓN DE IMPUESTOS">
			<div class="links last">
				<h4>PREPARACIÓN DE <br/> IMPUESTOS</h4>
				<a href="services#service3" class="btn2">Learn more</a>
			</div>
		</div>
	</div>
</div>
<div id="house">
	<div class="row">
		<div class="text">
			<p>Free Electronic <br/>Filing</p>
		</div>
		<div class="text">
			<p>Nuestro compromiso con usted será un servicio de excelente calidad.</p>
		</div>
		<div class="text">
			<p>Nuestra meta es la de formar una relación permanente con usted: nuestro cliente.</p>
		</div>
	</div>
	<img src="public/images/common/house.jpg" alt="house" class="house">
</div>
<div id="what">
	<div class="row">
		<div class="panel">
			<h2>What <span>They Say</span></h2>
			<p class="comment">Rendon tax services saved my dad over $2000 on his tax return. They are extremely kind and professional and eager to earn your business. They have our business and loyalty!</p>
			<p class="author">Sondra B.</p>
		</div>
	</div>
</div>
